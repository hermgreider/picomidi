#ifndef PMID_LED_H_
#define PMID_LED_H_

enum  {
  BLINK_NOT_MOUNTED = 250,
  BLINK_MOUNTED = 1000,
  BLINK_SUSPENDED = 2500,
};

void pmid_led_set_speed(uint32_t interval_ms);
void pmid_led_set_temp_flashes(uint8_t num, uint32_t interval_ms);

#endif 
