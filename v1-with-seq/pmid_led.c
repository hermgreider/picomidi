#include "bsp/board.h"

#include "pmid_timer.h"
#include "pmid_led.h"

static uint32_t blink_interval_ms;
static uint8_t timer_id;

static void timer_callback(void)
{
  static bool led_state = true;

  board_led_write(led_state);
  led_state = 1 - led_state; // toggle

  timer_id = pmid_timer_create(blink_interval_ms, timer_callback);
}

void pmid_led_set_speed(uint32_t interval_ms)
{
  blink_interval_ms = interval_ms;
  pmid_timer_cancel(timer_id);
  timer_id = pmid_timer_create(blink_interval_ms, timer_callback);
}

void pmid_led_set_temp_flashes(uint8_t num, uint32_t interval_ms)
{
}
