#ifndef PMID_CONTROLLER_H_
#define PMID_CONTROLLER_H_

void pmid_controller_init(void);
void pmid_controller_task(void);

#endif 