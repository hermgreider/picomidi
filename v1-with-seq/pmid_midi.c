#include "bsp/board.h"
#include "tusb.h"
#include "pmid_led.h"
#include "pmid_midi.h"

static PmidMidiCallback callbacks[NUM_CALLBACKS] = {};

static uint8_t packet[4] = {};
static uint8_t control_notes[3] = {};
static uint8_t current_note = 0;

static void pmid_mid_clear_news(void);

void pmid_mid_init()
{
  pmid_led_set_speed(BLINK_NOT_MOUNTED);
  tusb_init();
}

void pmid_mid_task() 
{
  tud_task(); // tinyusb device task

  if (tud_midi_available())
  {
    tud_midi_packet_read(packet);
    pmid_mid_clear_news();

    for (uint8_t i=0; i<NUM_CALLBACKS; i++) { 
      if (callbacks[i].callback_func == NULL) continue;
      
      // skip callbacks registered in another callback
      if (callbacks[i].new) {
        continue;
      }

      (callbacks[i].callback_func)(packet);
    }
  }
}

uint8_t pmid_mid_register(void (*callback_func)(uint8_t *packet))
{
  // Find an empty entry
  for (uint8_t i=0; i<NUM_CALLBACKS; i++) 
  { 
    if (callbacks[i].callback_func == NULL) 
    {
      callbacks[i].callback_func = callback_func;
      callbacks[i].new = true;
      return i;
    }
  }
}

// The "new" flag is not a great way to handle calling 
// a callback inside another callback. But, this hack works.
static void pmid_mid_clear_news(void) 
{
  for (uint8_t i=0; i<NUM_CALLBACKS; i++) { 
    callbacks[i].new = false;
  }
}

void pmid_mid_cancel(uint8_t id)
{
  callbacks[id].callback_func = NULL;
  callbacks[id].new = false;
}

void pmid_mid_packet(uint8_t const* buffer, uint32_t bufsize)
{
  tud_midi_stream_write(CABLE_NUM, buffer, bufsize);
}

void pmid_mid_note_on(uint8_t channel, uint8_t note, uint8_t velocity) 
{
  packet[0] = 0x90 | channel;
  packet[1] = note;
  packet[2] = velocity;
  tud_midi_stream_write(CABLE_NUM, packet, 3);
}

void pmid_mid_note_off(uint8_t channel, uint8_t note) 
{
  packet[0] = 0x80 | channel;
  packet[1] = note;
  packet[2] = 0;
  tud_midi_stream_write(CABLE_NUM, packet, 3);
}

// The MIDI interface always creates input and output port/jack descriptors
// regardless of these being used or not. Therefore incoming traffic should be read
// (possibly just discarded) to avoid the sender blocking in IO
void pmid_mid_flush_reads(void) 
{
  while ( tud_midi_available() ) tud_midi_packet_read(packet);
}


//--------------------------------------------------------------------+
// Tiny USB callbacks
//--------------------------------------------------------------------+

// Invoked when device is mounted
void tud_mount_cb(void)
{
  pmid_led_set_speed(BLINK_MOUNTED);
}

// Invoked when device is unmounted
void tud_umount_cb(void)
{
  pmid_led_set_speed(BLINK_NOT_MOUNTED);
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us  to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en)
{
  (void) remote_wakeup_en;
  pmid_led_set_speed(BLINK_SUSPENDED);
}

// Invoked when usb bus is resumed
void tud_resume_cb(void)
{
  pmid_led_set_speed(BLINK_MOUNTED);
}
