#ifndef PMID_STATUS_H_
#define PMID_STATUS_H_

enum  {
  PMID_SUCCESS = 0,
  PMID_SCALE = 1, 
  PMID_ERROR = 2,
  PMID_UP = 3,
  PMID_DOWN = 4,
  PMID_UPDOWN = 5,
  PMID_RANDOM = 6,
  PMID_OCTAVE1 = 7,
  PMID_OCTAVE2 = 8,
  PMID_OCTAVE3 = 9
};

void pmid_set_status(uint8_t channel_in, uint8_t status);
uint8_t pmid_status_task();

#endif 