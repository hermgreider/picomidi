#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bsp/board.h"
#include "pmid_controller.h"
#include "pmid_midi.h"
#include "pmid_timer.h"

/*------------- MAIN -------------*/
int main(void)
{
  board_init();

  printf("\nSTARTUP\n");

  pmid_mid_init();
  pmid_controller_init();

  while (1)
  {
    pmid_timer_task();
    pmid_mid_task();
  }

  return 0;
}
 