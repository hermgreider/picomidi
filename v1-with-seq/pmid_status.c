#include "bsp/board.h"
#include "pmid_timer.h"
#include "pmid_midi.h"
#include "pmid_status.h"

// Status is indicated by playing a sequence. Used in controller and each mode status states.
const uint16_t delay_msec = 150;

static uint8_t *current_sequence = NULL;
static uint8_t current_note = 0;
static uint8_t timer_id = 0;
static uint8_t channel = 0;

static uint8_t success[] = {60,60,60,72,0};
static uint8_t scale[] = {60,62,64,65,67,69,71,72,0};
static uint8_t error[] = {40,40,0};
static uint8_t up[] = {60,62,64,65,0};
static uint8_t down[] = {65,64,62,60,0};
static uint8_t updown[] = {60,62,64,65,64,62,60,0};
static uint8_t random[] = {60,68,65,62,64,63,0};
static uint8_t octave1[] = {60,0};
static uint8_t octave2[] = {60,72,0};
static uint8_t octave3[] = {60,72,84,0};

static uint8_t *sequence[] = { success, scale, error, up, down, updown, random, octave1, octave2, octave3 };

static void timer_callback(void);

void pmid_set_status(uint8_t channel_in, uint8_t status) 
{
  channel = channel_in;

  current_note = 0;
  current_sequence = sequence[status];

  timer_id = pmid_timer_create(delay_msec, timer_callback);
} 

static void timer_callback(void) 
{
  if (current_sequence == NULL) return;

  // printf("Timer callback, current note: %d\n", current_note);

  // Don't let user do anything while playing sequence
  pmid_mid_flush_reads();

  // Check for done (each sequence is 0-terminated)
  if (current_sequence[current_note] == 0) {
    pmid_mid_note_off(channel, current_sequence[current_note-1]);
    current_sequence = NULL;
    return;
  }

  // Play current note
  pmid_mid_note_on(channel, current_sequence[current_note], 127);

  // Turn off previous note
  int previous = current_note - 1;
  if (previous >= 0 && current_sequence[previous] != current_sequence[current_note]) {
    pmid_mid_note_off(channel, current_sequence[previous]);
  }

  // Increment position
  current_note++;

  timer_id = pmid_timer_create(delay_msec, timer_callback);
}
