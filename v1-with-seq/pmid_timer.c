
#include "bsp/board.h"
#include "pmid_timer.h"

static PmidTimerCallback callbacks[10] = {
  { 0, NULL}, { 0, NULL}, { 0, NULL}, { 0, NULL}, { 0, NULL}, { 0, NULL}, { 0, NULL}, { 0, NULL}, { 0, NULL}, { 0, NULL}
};

uint8_t pmid_timer_create(uint32_t delay_msec, void (*callback_func)(void))
{
  // Find an empty entry
  for (uint8_t i=0; i<10; i++) { 
    PmidTimerCallback *callback = &callbacks[i];
    if (callback->timeout_msec == 0) 
    {
      callback->timeout_msec = board_millis() + delay_msec;
      callback->callback_func = callback_func;
      return i;
    }
  }
}

void pmid_timer_cancel(uint8_t id)
{
  callbacks[id].timeout_msec = 0;
}

void pmid_timer_task(void) 
{
  for (uint8_t i=0; i<10; i++) {
    PmidTimerCallback *callback = &callbacks[i];
    if (callback->timeout_msec == 0) continue;
    if (board_millis() > callback->timeout_msec)
    {
      (callback->callback_func)();
      callback->timeout_msec = 0;
    }
  }
}
