#include "bsp/board.h"

#include "pmid_status.h"
#include "pmid_midi.h"
#include "pmid_timer.h"
#include "pmid_sequencer.h"

const uint16_t MAX_NOTES = 1024;

static struct Sequence {
  uint8_t status;
  uint8_t data1;
  uint8_t data2;
  uint32_t offset_ms; /* ms from start of the track */
} sequence[4][1024];

static uint8_t *control_notes = NULL;
static uint32_t start_ms = 0;
static uint8_t current_note = 0;

static int8_t midi_callback_id = -1;
static int8_t timer_callback_id = -1;

static uint8_t track = 0;
static uint16_t num_track_notes[4] = { 0, 0, 0, 0};

static void ready(uint8_t *packet);
static void stopped(uint8_t *packet);
static void reset(uint8_t *packet);
static void startstop(uint8_t *packet);
static void play(void);
static void clear_callbacks(void);

void pmid_sequencer_init(uint8_t *control_notes_in)
{
  control_notes = control_notes_in;
  midi_callback_id = pmid_mid_register(ready);
}

static void ready(uint8_t *packet)
{
  uint16_t current_seq_note = num_track_notes[track];
  if (current_seq_note == 0) { // IS THIS RIGHT IF start_ms IS FOR FIRST TRACK?
    start_ms = board_millis(); 
  }
  else if (current_seq_note == MAX_NOTES) {
    return; // track is full
  }
  
  // if start/stop, stop recording
  if (packet[2] == control_notes[1]) {
    pmid_mid_cancel(midi_callback_id);
    midi_callback_id = pmid_mid_register(stopped);
    printf("STOPPED sequence\n\n");
    return;
  }

  // Save note to sequence
  struct Sequence *sequence_note = &sequence[track][current_seq_note];
  sequence_note->status = packet[1];
  sequence_note->data1 = packet[2];
  sequence_note->data2 = packet[3];
  sequence_note->offset_ms = board_millis() - start_ms;

  ++num_track_notes[track];

  printf("READY, saved 0x%x, 0x%x, 0x%x\n", packet[1], packet[2], packet[3]);
}

static void stopped(uint8_t *packet)
{
  if (!pmid_mid_is_note_off(packet)) return;

  // Start 
  if (packet[2] == control_notes[1]) {
    pmid_mid_cancel(midi_callback_id);
    midi_callback_id = pmid_mid_register(startstop);
    
    current_note = 0;
    track = 0;
    start_ms = board_millis();

    // play first note - play sets up timer events
    play();
    
    printf("Start playing sequence\n");
    return;
  }

  // Reset 
  printf("STOPPED, check reset, note 0x%x, reset note 0x%x\n", packet[2], control_notes[0]);
  if (packet[2] == control_notes[0]) {
    pmid_mid_cancel(midi_callback_id);
    midi_callback_id = pmid_mid_register(reset);
    printf("Reset\n\n");
  }
}

// In Reset, wait for another Reset note. An "Are you sure?" requiring 
// two reset notes to actually reset.
static void reset(uint8_t *packet)
{
  if (!pmid_mid_is_note_off(packet)) return;

  // Reset 
  if (packet[2] == control_notes[0]) {
    pmid_set_status(0, PMID_SCALE);
    track = 0;
    for (uint8_t i=0; i<4; i++) {
      num_track_notes[i] = 0;
    }
    printf("Reset\n");
    return;
  }

  // Otherwise, any other note ignores reset and returns to READY
  pmid_mid_cancel(midi_callback_id);
  midi_callback_id = pmid_mid_register(reset);
  printf("Ignoring Reset\n");
}

// Watches for control note during sequence playback
static void startstop(uint8_t *packet)
{
  if (pmid_mid_is_note_off(packet)) {

    // if start/stop, stop playing. Otherwise, ignore any other notes.
    if (packet[2] == control_notes[1]) {
      clear_callbacks();
      midi_callback_id = pmid_mid_register(stopped);
      printf("User stopped sequence\n");
      return;
    }
  }
}
  
// Timer callback 
static void play(void)
{
  uint8_t packet_out[3] = { 
    sequence[track][current_note].status, 
    sequence[track][current_note].data1, 
    sequence[track][current_note].data2 
  };

  pmid_mid_packet(packet_out, 3);
  printf("PLAYED, current_note %d %x\n", current_note, sequence[track][current_note].data1);

  // Increment position
  current_note++;

  // If we are at the end of the sequence, go to STOPPED.
  if (current_note >= num_track_notes[track]) {
    clear_callbacks();
    midi_callback_id = pmid_mid_register(stopped);
    printf("Sequence complete\n\n");
  }
  // setup timer for next note
  else {
    uint32_t timerdelay = sequence[track][current_note].offset_ms - sequence[track][current_note-1].offset_ms;
    timer_callback_id = pmid_timer_create(timerdelay, play);
    printf("next note timer, current_note: %d, delaymsec: %d\n", current_note, timerdelay);
  }
}

static void clear_callbacks(void) 
{
  if (midi_callback_id >= 0) {
    pmid_mid_cancel(midi_callback_id);
  }
  if (timer_callback_id >= 0) {
    pmid_timer_cancel(timer_callback_id);  
  }
  midi_callback_id = -1;
  timer_callback_id = -1;
}