#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bsp/board.h"
#include "tusb.h"

void led_blinking_task(void);
void midi_task(void);
void set_state(uint8_t new_state);

enum  {
  POWERUP = 0,
  PLAY_SCALE = 1,
  READY = 2,
  STOPPED = 3,
  PLAY = 4,
  RESET = 5
};

/*------------- MAIN -------------*/
int main(void)
{
  board_init();
  tusb_init();

  set_state(POWERUP);
  printf("\nPOWERUP\n");

  while (1)
  {
    tud_task(); // tinyusb device task
    led_blinking_task();
    midi_task();
  }

  return 0;
}

//--------------------------------------------------------------------+
// MIDI Task
//--------------------------------------------------------------------+

const uint8_t cable_num = 0; // MIDI jack associated with USB endpoint
const uint16_t MAX_NOTES = 1024;

// Store example melody as an array of note values
uint8_t note_sequence[] =
{
  74,78,81,86,90,93,98,102,57,61,66,69,73,78,81,85,88,92,97,100,97,92,88,85,81,78,
  74,69,66,62,57,62,66,69,74,78,81,86,90,93,97,102,97,93,90,85,81,78,73,68,64,61,
  56,61,64,68,74,78,81,86,90,93,98,102
};

struct Sequence {
  uint8_t status;
  uint8_t data1;
  uint8_t data2;
  uint32_t offset_ms; /* ms from start of the track */
} sequence[4][1024];

uint8_t scale[] =
{
  60,62,64,65,67,69,71,72
};

uint8_t packet[4];
uint8_t control_notes[3];
uint8_t state = PLAY_SCALE;
uint8_t current_note = 0;
uint32_t start_ms = 0;
uint8_t channel = 0; // 0 for channel 1
uint8_t track = 0;
uint16_t num_track_notes[4] = { 0, 0, 0, 0};

void set_state(uint8_t new_state) {
  state = new_state;
  start_ms = board_millis();
  current_note = 0;
}

bool is_note_on (uint8_t packet[4]) {
  return (packet[1] & 0xf0) == 0x90;
}

uint8_t get_note(uint8_t packet[4]) {
  return packet[2];
}

uint8_t get_channel(uint8_t packet[4]) {
  return packet[1] & 0xf;
}

void play_note_on(uint8_t note, uint8_t velocity) {
  uint8_t note_on[3] = { 0x90 | channel, note, velocity };
  tud_midi_stream_write(cable_num, note_on, 3);
}

void play_note_off(uint8_t note) {
  uint8_t note_off[3] = { 0x80 | channel, note, 0};
  tud_midi_stream_write(cable_num, note_off, 3);
}

// The MIDI interface always creates input and output port/jack descriptors
// regardless of these being used or not. Therefore incoming traffic should be read
// (possibly just discarded) to avoid the sender blocking in IO
void flush_reads(void) {
   while ( tud_midi_available() ) tud_midi_packet_read(packet);
}

void midi_task(void) {

  switch (state) {

    case POWERUP:
      if (! tud_midi_available()) return;

      tud_midi_packet_read(packet);

      // User selects 3 notes as RESET, START/STOP, TRACK
      // Only look for NOTE_ON - exclude NOTE_OFF and CC for POWERUP sequence
      if (!is_note_on(packet)) return;

      control_notes[current_note] = get_note(packet);
      channel = get_channel(packet);
      printf("POWERUP, control note: 0x%x, channel: 0x%x\n", control_notes[current_note], channel);
      if (++current_note == 3) {
        set_state(PLAY_SCALE);
        printf("POWERUP complete\n");
      }
      break;

    case PLAY_SCALE:

      flush_reads();

      // send note periodically
      if (board_millis() - start_ms < 100) return; // not enough time
      start_ms += 100;

      // Previous positions in the note sequence.
      int previous = current_note - 1;

      // If we currently are at position 0, set the
      // previous position to the last note in the sequence.
      if (previous < 0) previous = sizeof(scale) - 1;

      play_note_on(scale[current_note], 127);
      play_note_off(scale[previous]);

      // Increment position
      current_note++;

      // If we are at the end of the sequence, start over.
      if (current_note >= sizeof(scale)) {
        play_note_off(scale[current_note-1]);
        set_state(READY);
        printf("Ready for note\n");
      }
  
      break;

    case READY:
      if (! tud_midi_available()) return;

      tud_midi_packet_read(packet);

      uint16_t current_seq_note = num_track_notes[track];
      if (current_seq_note == 0) {
        start_ms = board_millis();
      }
      else if (current_seq_note == MAX_NOTES) {
        return; // track is full
      }
      
      // if start/stop, stop recording
      // printf("READY, packet[2] 0x%x, control_notes[1] 0x%x\n", packet[2], control_notes[1]);
      if (packet[2] == control_notes[1]) {
        set_state(STOPPED);
        printf("STOPPED sequence\n");
        return;
      }

      struct Sequence *sequence_note = &sequence[track][current_seq_note];
      sequence_note->status = packet[1];
      sequence_note->data1 = packet[2];
      sequence_note->data2 = packet[3];
      sequence_note->offset_ms = board_millis() - start_ms;

      ++num_track_notes[track];

      printf("READY, saved 0x%x, 0x%x, 0x%x\n", packet[1], packet[2], packet[3]);
      break;

    case STOPPED:
      if (! tud_midi_available()) return;

      tud_midi_packet_read(packet);

      if (!is_note_on(packet)) return;

      // Start 
      if (packet[2] == control_notes[1]) {
        set_state(PLAY);
        printf("Start playing sequence\n");
        return;
      }

      // Reset 
      printf("STOPPED, check reset, note 0x%x, reset note 0x%x\n", packet[2], control_notes[0]);
      if (packet[2] == control_notes[0]) {
        set_state(RESET);
        printf("Reset\n");
      }
      break;

    // In Reset, wait for another Reset note. An "Are you sure?" requiring 
    // two reset notes to actually reset.
    case RESET:
      if (! tud_midi_available()) return;

      tud_midi_packet_read(packet);

      if (!is_note_on(packet)) return;

      // Reset 
      if (packet[2] == control_notes[0]) {
        set_state(PLAY_SCALE);
        track = 0;
        for (uint8_t i=0; i<4; i++) {
          num_track_notes[i] = 0;
        }
        printf("Reset\n");
        return;
      }

      // Otherwise, any other note ignores reset and returns to READY
      set_state(READY);
      printf("Ignoring Reset\n");

      break;

    case PLAY:
      if (tud_midi_available()) {
        tud_midi_packet_read(packet);

        if (is_note_on(packet)) {

          // if start/stop, stop playing. Otherwise, ignore any other notes.
          if (packet[2] == control_notes[1]) {
            set_state(STOPPED);
            printf("User stopped sequence\n");
            return;
          }

        }
      }

      // check for next note ready
      if (board_millis() - start_ms < sequence[track][current_note].offset_ms) {
        return; // not time for this note
      }

      // printf("PLAY, board_millis - start_ms %d, offset_ms %d\n", 
      //   board_millis() - start_ms, sequence[track][current_note].offset_ms);
      uint8_t packet_out[3] = { 
        sequence[track][current_note].status, 
        sequence[track][current_note].data1, 
        sequence[track][current_note].data2 
      };
      tud_midi_stream_write(cable_num, packet_out, 3);

      // Increment position
      current_note++;

      // printf("PLAY, after note, current_note %d, num_track_notes %d\n", current_note, num_track_notes[track]);

      // If we are at the end of the sequence, go to STOPPED.
      if (current_note >= num_track_notes[track]) {
        set_state(STOPPED);
        printf("Sequence complete\n");
      }
      break;
  }        
}

//--------------------------------------------------------------------+
// BLINKING TASK
//--------------------------------------------------------------------+

enum  {
  BLINK_NOT_MOUNTED = 250,
  BLINK_MOUNTED = 1000,
  BLINK_SUSPENDED = 2500,
};

static uint32_t blink_interval_ms = BLINK_NOT_MOUNTED;

// Device callbacks for blinking events

// Invoked when device is mounted
void tud_mount_cb(void)
{
  blink_interval_ms = BLINK_MOUNTED;
}

// Invoked when device is unmounted
void tud_umount_cb(void)
{
  blink_interval_ms = BLINK_NOT_MOUNTED;
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us  to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en)
{
  (void) remote_wakeup_en;
  blink_interval_ms = BLINK_SUSPENDED;
}

// Invoked when usb bus is resumed
void tud_resume_cb(void)
{
  blink_interval_ms = BLINK_MOUNTED;
}

void led_blinking_task(void)
{
  static uint32_t start_ms = 0;
  static bool led_state = false;

  // Blink every interval ms
  if ( board_millis() - start_ms < blink_interval_ms) return; // not enough time
  start_ms += blink_interval_ms;

  board_led_write(led_state);
  led_state = 1 - led_state; // toggle
}
