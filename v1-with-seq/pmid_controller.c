#include "bsp/board.h"
#include "tusb.h"

#include "pmid_status.h"
#include "pmid_midi.h"
#include "pmid_led.h"
#include "pmid_sequencer.h"
#include "pmid_arp.h"

static uint8_t control_notes[3] = { 0, 0, 0 };
static uint8_t current_note = 0;
static uint8_t channel = 0;
static uint8_t callback_id = 0;

static void powerup(uint8_t *packet);
static void next_mode(uint8_t *packet);

/*------------- MAIN -------------*/
void pmid_controller_init(void)
{
  callback_id = pmid_mid_register(powerup);
}

//modes = { pmid_recorder_init, pmid_sequencer_init };

// get 3 control notes then play scale
static void powerup(uint8_t *packet)
{
  // User selects 3 notes as RESET, START/STOP, TRACK
  // Only look for NOTE_OFF - exclude NOTE_ON and CC for POWERUP sequence
  if (!pmid_mid_is_note_off(packet)) return;
  // printf("\nPOWERUP note off, %x, %x, %x, %x\n", packet[0], packet[1], packet[2], packet[3]);

  control_notes[current_note] = pmid_mid_get_note(packet);
  channel = pmid_mid_get_channel(packet);
  printf("POWERUP, control note: 0x%x, channel: 0x%x\n", control_notes[current_note], channel);

  if (++current_note == 3) {
    pmid_mid_cancel(callback_id);
    // pmid_mid_register(next_mode);

    pmid_set_status(channel, PMID_SUCCESS);
    // pmid_set_status(PMID_SCALE);

    // pmid_sequencer_init(control_notes);
    pmid_arp_init(control_notes);

    // start the next mode
    //    modes[0]()
    
    printf("POWERUP complete\n\n");
  }
}

// detect a next mode sequence of RESET and CONTROL2
static void next_mode(uint8_t *packet)
{
  static uint8_t notes_down = 0;

  // Check for RESET and CONTROL2 notes down
  
  // if note on and RESET or CONTROL2, save it
  if (pmid_mid_is_note_on(packet) &&
    (packet[2] == control_notes[0] ||
     packet[2] == control_notes[2]))
  {
    notes_down++;
    if (notes_down == 2)
    {
      printf("Next Mode");
    }
  }
  
  // if (pmid_mid_is_note_off(packet) &&
  //   (packet[2] == control_notes[0] ||
  //    packet[2] == control_notes[2]))
  // {
  //   notes_down--;
  // }
}