#ifndef PMID_ARP_H_
#define PMID_ARP_H_

typedef struct {
  uint8_t status;
  uint8_t data1;
  uint8_t data2;
  uint32_t offset_ms; /* ms from start of the track */
} Note;

enum pattern {
  UP = 0, DOWN = 1, UPDOWN = 2, RANDOM = 3, LAST_PATTERN = 4
};

void pmid_arp_init(uint8_t *control_notes_in);

#endif 
