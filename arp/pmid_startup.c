#include "bsp/board.h"
#include "tusb.h"

#include "pmid_status.h"
#include "pmid_midi.h"
#include "pmid_arp.h"
#include "pmid_notes.h"
#include "pmid_pattern.h"
#include "pmid_octaves.h"
#include "pmid_swing.h"

/*
 * Wait for first control note and initialize the rest of the app
 */

static uint8_t callback_id = 0;
static void init_app(uint8_t *packet);

/*------------- MAIN -------------*/
void pmid_startup_init(void)
{
  callback_id = pmid_mid_register_any(init_app);
}

// get 3 control notes then play scale
static void init_app(uint8_t *packet)
{
  static uint8_t first_note;
  
  // Only look for NOTE_OFF - exclude NOTE_ON and CC for POWERUP sequence
  if (!pmid_mid_is_note_off(packet)) return;

  printf("\nSTARTUP note off, %x, %x, %x, %x\n", packet[0], packet[1], packet[2], packet[3]);

  first_note = pmid_mid_get_note(packet);

  pmid_mid_cancel(callback_id);
  pmid_set_status(PMID_SUCCESS);

	pmid_notes_init(first_note, first_note+6);
	pmid_pattern_init(first_note);
	pmid_notes_hold_init(first_note+1);
  pmid_octaves_init(first_note+2);
  pmid_swing_init(first_note+3);
  pmid_arp_speed_init(first_note+5, first_note+4);

  printf("STARTUP complete\n\n");
}