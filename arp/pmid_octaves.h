#ifndef PMID_OCTAVES_H_
#define PMID_OCTAVES_H_

void pmid_octaves_init(uint8_t control_note);
uint8_t pmid_current_octaves();

#endif 