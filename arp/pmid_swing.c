#include "bsp/board.h"
#include "pmid_status.h"
#include "pmid_midi.h"
#include "pmid_swing.h"

static uint8_t swing = STRAIGHT;

static void increment_swing(uint8_t *packet);

void pmid_swing_init(uint8_t control_note)
{
  // listen for swing changes
  pmid_mid_register_note(control_note, increment_swing);
}

uint8_t pmid_swing_current(void)
{
  return swing;
}

/*
 * Callback for Swing Control Note
 */
static void increment_swing(uint8_t *packet)
{
  if (pmid_mid_is_note_off(packet)) {

    // Increment to next swing pattern 
    swing = (swing + 1) % LAST_SWING;
    // pmid_set_status(TODO: need a timing pattern in status!); 
    
    printf("Swing %d chosen\n", swing);
  }
}
