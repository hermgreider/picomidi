#include "bsp/board.h"
#include "tusb.h"
#include "pmid_led.h"
#include "pmid_midi.h"

static PmidMidiCallback callbacks[NUM_CALLBACKS] = {};

static uint8_t packet[4] = {};
static uint8_t control_notes[3] = {};
static uint8_t current_note = 0;
static uint8_t channel = 255;

static void clear_news(void);

void pmid_mid_init()
{
  pmid_led_set_speed(BLINK_NOT_MOUNTED);
  tusb_init();
}

// for each note, cycle through listeners looking for a match. Always ignore CC.
void pmid_mid_task() 
{
  static uint8_t msg_channel;
  static uint8_t msg_note;

  tud_task(); // tinyusb device task

  clear_news();

  if (tud_midi_available())
  {
    tud_midi_packet_read(packet);

    msg_channel = pmid_mid_get_channel(packet);
    if (channel == 255) {
      channel = msg_channel;
      printf("STARTUP channel: 0x%x\n", channel);
    }

    // Verify that message is for us
    else if (channel != msg_channel) {
      return;
    }

    // Exclude CC
    else if (pmid_mid_is_cc(packet)) {
      return;
    }

    // Check each callback for match and send packet if message matches
    msg_note = pmid_mid_get_note(packet);
    for (uint8_t i=0; i<NUM_CALLBACKS; i++) { 
      if (callbacks[i].callback_func == NULL) continue;

      // avoid calling a newly registered callback inside another callback
      if (callbacks[i].new) continue; 

      // any note callback
      if (callbacks[i].note1 == 0) {
        (callbacks[i].callback_func)(packet);
      }
      // single note callback
      else if (callbacks[i].note2 == 0) {
        if (callbacks[i].note1 == msg_note) {
          (callbacks[i].callback_func)(packet);
        }
      }
      // exclude range callback
      else if ((msg_note < callbacks[i].note1) || (msg_note > callbacks[i].note2)) {
        (callbacks[i].callback_func)(packet);
      }
    }
  }
}

// send any note to callback
uint8_t pmid_mid_register_any(void (*callback_func)(uint8_t *packet))
{
  // Find an empty entry
  for (uint8_t i=0; i<NUM_CALLBACKS; i++) 
  { 
    if (callbacks[i].callback_func == NULL) 
    {
      callbacks[i].note1 = 0;
      callbacks[i].note2 = 0;
      callbacks[i].new = true;
      callbacks[i].callback_func = callback_func;
      return i;
    }
  }
}

// register for specific note
uint8_t pmid_mid_register_note(uint8_t note, void (*callback_func)(uint8_t *packet))
{
  // Find an empty entry
  for (uint8_t i=0; i<NUM_CALLBACKS; i++) 
  { 
    if (callbacks[i].callback_func == NULL) 
    {
      callbacks[i].note1 = note;
      callbacks[i].note2 = 0;
      callbacks[i].new = true;
      callbacks[i].callback_func = callback_func;
      return i;
    }
  }
}

// send any note not in range
uint8_t pmid_mid_register_exclude_range(uint8_t first_note, uint8_t last_note, void (*callback_func)(uint8_t *packet))
{
  // Find an empty entry
  for (uint8_t i=0; i<NUM_CALLBACKS; i++) 
  { 
    if (callbacks[i].callback_func == NULL) 
    {
      callbacks[i].note1 = first_note;
      callbacks[i].note2 = last_note;
      callbacks[i].new = true;
      callbacks[i].callback_func = callback_func;
      return i;
    }
  }
}

void pmid_mid_cancel(uint8_t id)
{
  callbacks[id].callback_func = NULL;
}

void pmid_mid_packet(uint8_t const* buffer, uint32_t bufsize)
{
  tud_midi_stream_write(CABLE_NUM, buffer, bufsize);
}

void pmid_mid_note_on(uint8_t note, uint8_t velocity) 
{
  packet[0] = 0x90 | channel;
  packet[1] = note;
  packet[2] = velocity;
  tud_midi_stream_write(CABLE_NUM, packet, 3);
}

void pmid_mid_note_off(uint8_t note) 
{
  packet[0] = 0x80 | channel;
  packet[1] = note;
  packet[2] = 0;
  tud_midi_stream_write(CABLE_NUM, packet, 3);
}

// The MIDI interface always creates input and output port/jack descriptors
// regardless of these being used or not. Therefore incoming traffic should be read
// (possibly just discarded) to avoid the sender blocking in IO
void pmid_mid_flush_reads(void) 
{
  while ( tud_midi_available() ) tud_midi_packet_read(packet);
}

// send any note not in range
static void clear_news(void) 
{
  // Find an empty entry
  for (uint8_t i=0; i<NUM_CALLBACKS; i++) 
  { 
    callbacks[i].new = false;
  }
}

//--------------------------------------------------------------------+
// Tiny USB callbacks
//--------------------------------------------------------------------+

// Invoked when device is mounted
void tud_mount_cb(void)
{
  pmid_led_set_speed(BLINK_MOUNTED);
}

// Invoked when device is unmounted
void tud_umount_cb(void)
{
  pmid_led_set_speed(BLINK_NOT_MOUNTED);
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us  to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en)
{
  (void) remote_wakeup_en;
  pmid_led_set_speed(BLINK_SUSPENDED);
}

// Invoked when usb bus is resumed
void tud_resume_cb(void)
{
  pmid_led_set_speed(BLINK_MOUNTED);
}
