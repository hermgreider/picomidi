#ifndef PMID_SEQUENCE_H_
#define PMID_SEQUENCE_H_

void pmid_sequence_create(void);
Note *pmid_sequence_current(void);
uint16_t pmid_sequence_num_notes(void);
void pmid_sequence_print(void);

#endif 