#ifndef PMID_NOTES_H_
#define PMID_NOTES_H_

typedef struct {
  uint8_t status;
  uint8_t data1;
  uint8_t data2;
  uint32_t offset_ms; /* ms from start of the track */
} Note;

void pmid_notes_init(uint8_t first_control, uint8_t last_control);
void pmid_notes_hold_init(uint8_t control_note);
bool pmid_notes_playing(void);
Note *pmid_notes_get(void); 
uint8_t pmid_notes_num_notes(void);
void pmid_notes_print(void);

#endif 