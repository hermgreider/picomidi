#include "bsp/board.h"
#include "pico/stdlib.h"

#include "pmid_midi.h"
#include "pmid_notes.h"
#include "pmid_sequence.h"
#include "pmid_pattern.h"
#include "pmid_swing.h"
#include "pmid_octaves.h"
#include "pmid_arp.h"

static bool start_new_sequence = false;
static uint64_t next_note_64 = 0;
static uint16_t notes_per_min = 300;

static void play(void);
static void play_note(Note note); 
static void speed_up(uint8_t *packet);
static void speed_down(uint8_t *packet);

/********************************
 * Public 
 ********************************/

void pmid_arp_task(void)
{
  static bool playing = false;
  
  if (!playing) {
    if (pmid_notes_num_notes() > 0) {
      // start on first note
      start_new_sequence = true;
      playing = true;

      // wait 30ms for additional notes to start sequence
      next_note_64 = time_us_64() + 30000;
    }
  }

  else if (pmid_notes_num_notes() == 0) {
    playing = false;    
  }

  else if (next_note_64 < time_us_64()) {
    play();
  }
}

void pmid_arp_speed_init(uint8_t speed_up_control, uint8_t speed_down_control)
{
  pmid_mid_register_note(speed_up_control, speed_up);
  pmid_mid_register_note(speed_down_control, speed_down);
}

/********************************
 * Callbacks
 ********************************/

static void speed_up(uint8_t *packet)
{
  if (pmid_mid_is_note_off(packet)) {
    notes_per_min += 10;
  }
}

static void speed_down(uint8_t *packet)
{
  if (pmid_mid_is_note_off(packet)) {
    notes_per_min -= 10;
  }
}

/********************************
 * Play
 ********************************/
static void play(void)
{
  static uint8_t pattern;
  static Note * sequence;
  static uint16_t num_sequence_notes;
  static int8_t current_play_note = 0; // index of next note to play in sequence
  static int8_t direction = 0;
  static int8_t swing_num = 0;

  if (start_new_sequence) {

    pattern = pmid_pattern_current();
    start_new_sequence = false;

    swing_num = 0;
    if (pmid_swing_current() == PERCENT_66) {
      swing_num = 1;
    }

    pmid_sequence_create();
    sequence = pmid_sequence_current();
    num_sequence_notes = pmid_sequence_num_notes();

    if ((pattern == UP) || (pattern == UPDOWN) || (pattern == RANDOM)) {
      current_play_note = 0;
      direction = 1;
    }
    else if (pattern == DOWN) {
      current_play_note = num_sequence_notes - 1;
      direction = -1;
    }
  }

  play_note(sequence[current_play_note]);

  current_play_note += direction;

  printf("pattern: %d, current_play_note: %d, direction: %d\n", pattern, current_play_note, direction);
  if ((pattern == UP || pattern == RANDOM) && (current_play_note >= num_sequence_notes)) {
    start_new_sequence = 1;
  }
  else if ((pattern == DOWN || (pattern == UPDOWN && direction == -1)) && (current_play_note == -1)) {
    start_new_sequence = 1;
  }
  else if ((pattern == UPDOWN && direction == 1) && (current_play_note >= num_sequence_notes)) {
    current_play_note = current_play_note - 1; // repeat top
    direction = -1;
  }

  if (pmid_swing_current() == STRAIGHT) {
    next_note_64 += 60000000 / notes_per_min;
  }
  else {
    // first note is 33% of 2 notes so 67% of one
    // second note is 67% of 2 notes so 1.34 of one
    // For 66, sequence starts with swing_num = 1 so reversed

    next_note_64 += (60000000 / notes_per_min) * (67 * (swing_num+1)) / 100;
    swing_num = (swing_num + 1) % 2;
  }

  if (start_new_sequence) {
    printf("Created new sequence from notes: \n");
    pmid_notes_print();
    printf("Sequence: \n");
    pmid_sequence_print();
  }
}

/*
 * Play a note in notes
 * current_play_note is the index of the note to play
 */
static void play_note(Note note) 
{
  // Play the next note
  uint8_t packet_out[3] = { 
    note.status, 
    note.data1, 
    note.data2 
  };

  pmid_mid_packet(packet_out, 3);
  printf("PLAY, played 0x%x, 0x%x, 0x%x\n", packet_out[1], packet_out[2], packet_out[3]);
}

