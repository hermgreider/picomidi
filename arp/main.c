#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "bsp/board.h"
#include "pmid_startup.h"
#include "pmid_midi.h"
#include "pmid_timer.h"
#include "pmid_arp.h"

/*------------- MAIN -------------*/
int main(void)
{
  board_init();
  srand(time(NULL)); 

  printf("\nSTARTUP\n");

  pmid_mid_init();
  pmid_startup_init();

  while (1)
  {
    pmid_arp_task();
    pmid_timer_task();
    pmid_mid_task();
  }

  return 0;
}
 