#ifndef PMID_TIMER_H_
#define PMID_TIMER_H_

typedef struct PmidTimerCallback {
  uint32_t timeout_msec;
  void (*callback_func)(void);
} PmidTimerCallback;

uint8_t pmid_timer_create(uint32_t delay_msec, void (*callback_func)(void));
void pmid_timer_cancel(uint8_t id);
void pmid_timer_task(void);

#endif 