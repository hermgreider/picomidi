#ifndef PMID_PATTERN_H_
#define PMID_PATTERN_H_

enum pattern {
  UP = 0, DOWN = 1, UPDOWN = 2, RANDOM = 3, LAST_PATTERN = 4
};

void pmid_pattern_init(uint8_t control_note);
uint8_t pmid_pattern_current();

#endif 