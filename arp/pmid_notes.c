#include "bsp/board.h"
#include "pmid_midi.h"
#include "pmid_notes.h"

const uint8_t MAX_ARP_NOTES = 16;

static Note notes[16]; // List of currently played notes (note on's)
static uint8_t num_notes = 0; // number of current notes on
static int8_t midi_callback_id = -1;
static bool hold = 0;

static void new_note(uint8_t *packet);
static void toggle_hold(uint8_t *packet);
static void insert_note(Note note);
static void remove_note(uint8_t note_value);

void pmid_notes_init(uint8_t first_control, uint8_t last_control)
{
  pmid_mid_register_exclude_range(first_control, last_control, new_note);
}

void pmid_notes_hold_init(uint8_t control_note)
{
  pmid_mid_register_note(control_note, toggle_hold);
}

bool pmid_notes_playing(void) 
{
  return num_notes > 0;
}

Note *pmid_notes_get(void) 
{
  return notes;
}

uint8_t pmid_notes_num_notes(void) 
{
  return num_notes;
}

static void new_note(uint8_t *packet)
{
  if (pmid_mid_is_note_on(packet)) {

    // Add new note 
    static Note note;
    note.status = packet[1];
    note.data1 = packet[2];
    note.data2 = packet[3];

    insert_note(note);

    printf("READY, saved 0x%x, 0x%x, 0x%x\n", packet[1], packet[2], packet[3]);
  }

  else if (pmid_mid_is_note_off(packet)) {

    // Remove note from notes list
    remove_note(packet[2]);

    printf("READY, removed 0x%x, 0x%x, 0x%x\n", packet[1], packet[2], packet[3]);
  }
  
  pmid_notes_print();
}

/*
 * Callback for Hold Control Note
 */
static void toggle_hold(uint8_t *packet)
{
  if (pmid_mid_is_note_off(packet)) {
    hold = !hold;
    if (!hold) {
      num_notes = 0;
    }
  }
}

// Insert into sorted array
static void insert_note(Note note) 
{
  static uint32_t last_insert_time;
  static int8_t i;

  // Clear existing notes if hold is on and new set of notes played
  if (hold && last_insert_time + 100 < board_millis()) {
    num_notes = 0;
  }

  for (i = num_notes-1; (i >= 0 && notes[i].data1 > note.data1); i--)
      notes[i+1] = notes[i];

  notes[i+1] = note;
  ++num_notes;

  last_insert_time = board_millis();
}

// Delete from sorted array
static void remove_note(uint8_t note_value) 
{
  static int8_t i, j;

  if (hold) return;

  for (i = 0; (i < num_notes && notes[i].data1 != note_value); i++);

  for (j = i; (j < num_notes); j++) {
    notes[j] = notes[j+1];
  }

  --num_notes;
}

void pmid_notes_print(void) 
{
  if (num_notes == 0) {
    printf("No notes on\n\n");
    return;
  }

  printf("Print notes, num_notes: %d\n", num_notes);
  // for (uint8_t i=0; i<MAX_ARP_NOTES; i++)
  for (uint8_t i=0; i<num_notes; i++)
  {
    printf("  notes[%d] = %x\n", i, notes[i].data1);
  }
}