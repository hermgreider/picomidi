#ifndef PMID_SWING_H_
#define PMID_SWING_H_

enum swing_pattern {
  STRAIGHT = 0, PERCENT_33 = 1, PERCENT_66 = 2, LAST_SWING = 3
};

void pmid_swing_init(uint8_t control_note);
uint8_t pmid_swing_current();

#endif 