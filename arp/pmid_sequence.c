#include "bsp/board.h"
#include "pmid_pattern.h"
#include "pmid_octaves.h"
#include "pmid_pattern.h"
#include "pmid_notes.h"

#include <stdlib.h>
// #include "pico/stdlib.h"

static Note sequence[1024];
static uint16_t num_sequence_notes = 0;

static void randomize_sequence();

void pmid_sequence_create(void)
{
  static uint8_t octave;
  static uint8_t note_index;
  static Note *notes;

  notes = pmid_notes_get();
  num_sequence_notes = 0;
  for (octave=0; octave<pmid_current_octaves(); octave++) {
    for (note_index=0; note_index<pmid_notes_num_notes(); note_index++) {

      static Note seq_note;
      seq_note.status = notes[note_index].status;
      seq_note.data1 = notes[note_index].data1 + (octave * 12);
      seq_note.data2 = notes[note_index].data2;

      sequence[num_sequence_notes++] = seq_note;
    }
  }

  if (pmid_pattern_current() == RANDOM) {
    randomize_sequence();
  }
}

static void randomize_sequence() 
{
  static uint16_t i, j;
  static Note temp;

  for (i = num_sequence_notes-1; i >= 1; i--) 
  {
    j = rand() / (RAND_MAX / i + 1);

    temp = sequence[j];
    sequence[j] = sequence[i];
    sequence[i] = temp;
  }
}

Note *pmid_sequence_current(void)
{
  return sequence;
}

uint16_t pmid_sequence_num_notes(void)
{
  return num_sequence_notes;
}

void pmid_sequence_print(void) 
{
  if (num_sequence_notes == 0) {
    printf("No notes in sequence");
    return;
  }
  for (uint8_t i=0; i<num_sequence_notes; i++)
  {
    printf("  sequence[%d] = %x\n", i, sequence[i].data1);
  }
}