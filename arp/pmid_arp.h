#ifndef PMID_ARP_H_
#define PMID_ARP_H_

void pmid_arp_task(void);
void pmid_arp_speed_init(uint8_t speed_up_control, uint8_t speed_down_control);

#endif 
