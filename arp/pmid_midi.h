#ifndef PMID_MIDI_H_
#define PMID_MIDI_H_

typedef struct PmidMidiCallback {
  uint8_t note1;
  uint8_t note2;
  bool new;
  void (*callback_func)(uint8_t *packet);
} PmidMidiCallback;

void pmid_mid_init(void);
void pmid_mid_task(void);

uint8_t pmid_mid_register_any(void (*callback_func)(uint8_t *packet));
uint8_t pmid_mid_register_note(uint8_t note, void (*callback_func)(uint8_t *packet));
uint8_t pmid_mid_register_exclude_range(uint8_t first_note, uint8_t last_note, void (*callback_func)(uint8_t *packet));
void pmid_mid_cancel(uint8_t id);

#define CABLE_NUM 0 // MIDI jack associated with USB endpoint
#define NUM_CALLBACKS 10

void pmid_mid_note_on(uint8_t note, uint8_t velocity);
void pmid_mid_packet(uint8_t const* buffer, uint32_t bufsize);
void pmid_mid_note_off(uint8_t note);
void pmid_mid_flush_reads(void);

static inline bool pmid_mid_is_note_on (uint8_t *packet)
{
  return (packet[1] & 0xf0) == 0x90;
}

static inline bool pmid_mid_is_note_off (uint8_t *packet)
{
  return (packet[1] & 0xf0) == 0x80;
}

static inline bool pmid_mid_is_cc (uint8_t *packet)
{
  return (packet[1] & 0xf0) == 0xB0;
}

static inline uint8_t pmid_mid_get_note(uint8_t *packet) 
{
  return packet[2];
}

static inline uint8_t pmid_mid_get_channel(uint8_t *packet) 
{
  return packet[1] & 0xf;
}

#endif 