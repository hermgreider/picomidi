#include "bsp/board.h"
#include "pmid_status.h"
#include "pmid_midi.h"
#include "pmid_pattern.h"

static uint8_t pattern_status[] = { PMID_UP, PMID_DOWN, PMID_UPDOWN, PMID_RANDOM };
static uint8_t pattern = UP;

static void increment_pattern(uint8_t *packet);

void pmid_pattern_init(uint8_t control_note)
{
  // listen for pattern changes
  pmid_mid_register_note(control_note, increment_pattern);
}

uint8_t pmid_pattern_current(void)
{
  return pattern;
}

/*
 * Callback for Pattern Control Note
 */
static void increment_pattern(uint8_t *packet)
{
  if (pmid_mid_is_note_off(packet)) {

    // Increment to next pattern and play a sample of it
    pattern = (pattern + 1) % LAST_PATTERN;
    pmid_set_status(pattern_status[pattern]);
    
    printf("Pattern %d chosen\n", pattern);
  }
}
