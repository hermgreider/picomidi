#include "bsp/board.h"
#include "pmid_status.h"
#include "pmid_midi.h"
#include "pmid_pattern.h"

static uint8_t octaves = 1;

static void increment_octaves(uint8_t *packet);

void pmid_octaves_init(uint8_t control_note)
{
  // listen for octaves changes
  pmid_mid_register_note(control_note, increment_octaves);
}

uint8_t pmid_current_octaves()
{
  return octaves;
}

/*
 * Callback for Octaves Control Note
 */
static void increment_octaves(uint8_t *packet)
{
  static uint8_t octaves_status[] = { PMID_OCTAVE1, PMID_OCTAVE2, PMID_OCTAVE3 };
  
  if (pmid_mid_is_note_off(packet)) {

    // Increment octaves and play a sample of it
    ++octaves;
    if (octaves == 4) octaves = 1;
    
    pmid_set_status(octaves_status[octaves-1]);
    
    printf("Octaves %d chosen\n", octaves);
  }
}