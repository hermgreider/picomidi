First Setup

Using https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf
1. Download pico-sdk
2. 9.1.1 Install toolchain
3. 9.1.3 Install CMake Tools extension

Create Project
1. Create picomidi folder
2. Add main.c, tusb_config.h, usb_descriptors.c from https://github.com/hathach/tinyusb/tree/master/examples/device/midi_test/src
3. Create CMakeLists.txt from section 8.0 in Getting started
4. cp ../pico-sdk/external/pico_sdk_import.cmake .

Build
1. mkdir build 
2. cd build
3. cmake ..
4. make

Copy to pico
cp picomidi.uf2 /Volumes/RPxxx
