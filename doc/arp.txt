Arpeggiator

Similar to Jupiter 8 arp
	• Pattern - up, down, up/down, random
	• Repeat notes - 1, 2, 3
	• Octave - 1, 2, 3
	• Hold Mode

Keys
	• Normal 3 keys - Reset, Control 1, Control 2

Setup
	• Reset
	• Choose pattern
		○ Ctl 1 to cycle through patterns
		○ Ctl 2 for Done
		○ With each pattern, Arp confirms with up scale, down scale, up/down scale, random
	• Choose repeat notes
		○ Ctl 1 to cycle through repeats
		○ Ctl 2 for Done
		○ Confirm with 1, 2, or 3 notes
	• Choose Octaves
		○ Ctl 1 to cycle through 1, 2, 3
		○ Ctl 2 for Done
		○ Confirm with 1, 2, or 3 octaves
	• Choose BPM
		○ At any time
		○ Starts at 60
		○ Press Ctl 1 to increase BPM by 10
		○ Press Ctl 2 for Done
	
Operation
	• Play a chord
	• Arp plays pattern - lowest to highest note is sequence
	• If you add a note after 20 ms, it is tacked on the end
	• Press Ctl 1 to activate/deactivate Hold mode
	
Code

Default setup 
	Up, 2 octaves, Repeat 1, BPM 60
	State=ARP_READY # later, ARP_SETUP
	
ARP_READY 
	On Ctl1
		BPM = BPM + 10
	On Ctl2
		BPM = BPM + 1
	On NOTE_ON
		Save note
		state=ARP_RUN
		start_ms=millis()
		Current = 1
		Next_note = 0

ARP_RUN
	On Ctl1
		BPM = BPM + 10
	On Ctl2
		BPM = BPM + 1
	On NOTE_ON
		Save note
		Current++
	On NOTE_OFF
		Mark note to be removed
	
	// Start sequence 5ms after first note on to allow for all notes in chord to be recorded
	If (next_note == 0) && (millis() - start_ms > 5ms)
		Sort notes by value 
		Play notes[0]
		Next_note = 1
		Next_beat_ms = (next_note * 1000) / 60 
		return
	
	If (millis() - start_ms) < next_beat_ms) 
		// Not time yet
		return

	// Play next note
	Play notes[next_note++]
	Next_beat_ms = (next_note * 1000) / 60 
	If next_note == last_note
	Remove deleted notes
	Next note = 0
	
	If 
	
	• At each beat, play next note in sequence
	• Finish sequence before checking note list again
	• If all notes off and hold off, stop sequence
	• If Hold On, all notes off then new chord resets all notes to the new chord 
	
Main question
	• How to handle a chord played "at once?" It's not really at one time. Notes may come out of order. 
	• Wait 5 ms before starting sequence?
	• 1 message is ~ 1ms on hardware MIDI
